import os
from datetime import datetime
import json
import numpy as np
from PIL import Image
import torch
import imgclassifier.model as m

def predict_class(image_path, model, topk):
    ''' Predict the class (or classes) of an image using a trained deep learning model.
    '''
    im = Image.open(image_path)
    np_im = process_image(im)
    torch_im = torch.FloatTensor([np_im])
    model.eval()
    is_cuda = next(model.parameters()).is_cuda
    with torch.no_grad():
        if is_cuda:
            torch_im.to('cuda')
        output = model.forward(torch_im)
        output = torch.exp(output)
    probs, classes = output.cpu().topk(topk)
    return (probs.numpy()[0], classes.numpy()[0])

def normalize_color_channel(channel, mean, stddev):
    return ((channel/255) - mean) / stddev

def process_image(image):
    ''' Scales, crops, and normalizes a PIL image for a PyTorch model,
        returns an Numpy array
    '''
    image = image.resize((256,256))
    image = image.crop(box=(16, 16, 240, 240))
    np_image = np.array(image) / 255
    mean = np.array([0.485, 0.456, 0.406])
    std = np.array([0.229, 0.224, 0.225])
    np_image = (np_image - mean) / std
    np_image = np_image.transpose((2,0,1))
    return np_image

def make_fn_name_for_class(path_to_json_file):
    with open(path_to_json_file, 'r') as f:
        cat_to_name = json.load(f)
    return lambda c: cat_to_name.get('{}'.format(c+1))

